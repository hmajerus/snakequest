package util;

import boards.ObstacleList;
import game.Food;
import game.Game;
import game.SnakePart;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;


import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;


import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.Pair;
import org.springframework.security.crypto.bcrypt.BCrypt;
import screen.homeScreen;

import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
public class helperMethods {

    public static Player player;

    public static boolean loginLogic(String username, String password) {
        boolean success = false;
        String sql = "SELECT * FROM user WHERE username = ?";
        try {
            PreparedStatement preparedStatement = homeScreen.connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                success = BCrypt.checkpw(password, resultSet.getString("password"));
                if(success) {
                    player = new Player(resultSet.getString("username"),
                            resultSet.getInt("freePlayHighScore"),
                            resultSet.getInt("levelOneHighScore"),
                            resultSet.getInt("levelTwoHighScore"),
                            resultSet.getInt("levelThreeHighScore"),
                            resultSet.getInt("currLevel"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public static boolean registerLogic(String username, String password) {
        boolean success = false;
        String hashpw;
        String sql = "SELECT * FROM user WHERE username = ?";
        try {
            PreparedStatement preparedStatement = homeScreen.connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                hashpw = BCrypt.hashpw(password, BCrypt.gensalt());
                sql = "INSERT INTO user(username, password, currLevel) VALUES(?, ?, 1)";
                preparedStatement = homeScreen.connection.prepareStatement(sql);
                preparedStatement.setString(1, username);
                //noinspection JpaQueryApiInspection
                preparedStatement.setString(2, hashpw);
                preparedStatement.executeUpdate();      //Returns (int) but dont think we need it
                success = true;
                player = new Player(username,0,0,0,0, 1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public static void nextScreen(javafx.event.ActionEvent event, String screen) {
        Stage dialogStage;
        Scene scene;
        try {
            Node node = (Node) event.getSource();
            dialogStage = (Stage) node.getScene().getWindow();
            dialogStage.close();
            scene = new Scene(FXMLLoader.load(homeScreen.class.getResource(screen)));
            dialogStage.setScene(scene);
            dialogStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void infoBox(String infoMessage, String headerText, String title) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait();
    }

    public static int keyPressed(KeyEvent e) {
        if (e.getCode() == KeyCode.RIGHT) {
            return (0);
        } else if (e.getCode() == KeyCode.UP) {
            return (1);
        } else if (e.getCode() == KeyCode.LEFT) {
            return (2);
        } else if (e.getCode() == KeyCode.DOWN) {
            return (3);
        } else if(e.getCode() == KeyCode.SPACE) {
            return (4);
        }

        return -1;
    }


    public static int getHighScore(int level){
        String lvl = "";
        if(level == 1){
            lvl = "levelOneHighScore";
        } else if(level == 2) {
            lvl = "levelTwoHighScore";
        } else if(level == 3) {
            lvl = "levelThreeHighScore";
        } else if(level == 4){
            lvl = "levelFourHighScore";
        } else if(level == 5){
            lvl = "levelFiveHighScore";
        } else {
            lvl = "freePlayHighScore";
        }
        String sql = "SELECT * FROM user WHERE username = ?";
        int highScore = -1;
        try{
            PreparedStatement preparedStatement = homeScreen.connection.prepareStatement(sql);
            preparedStatement.setString(1, player.getPlayerName());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                highScore = resultSet.getInt(lvl);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return highScore;
    }

    public static void setHighScore(int level, int score){
        String lvl = "";
        if(level == 1){
            lvl = "levelOneHighScore";
        } else if(level == 2) {
            lvl = "levelTwoHighScore";
        } else if(level == 3) {
            lvl = "levelThreeHighScore";
        } else if(level == 4){
            lvl = "levelFourHighScore";
        } else if(level == 5){
            lvl = "levelFiveHighScore";
        } else {
            lvl = "freePlayHighScore";
        }
        String sql = "UPDATE user SET " + lvl + " = ? WHERE username = ?";
        try {
            PreparedStatement preparedStatement = homeScreen.connection.prepareStatement(sql);
            preparedStatement.setString(1, score + "");
            preparedStatement.setString(2, player.getPlayerName());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setCurrentLevel(int level){
        String sql = "UPDATE user SET currLevel = ? WHERE username = ?";
        try {
            PreparedStatement preparedStatement = homeScreen.connection.prepareStatement(sql);
            preparedStatement.setString(1, level + "");
            preparedStatement.setString(2, player.getPlayerName());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @FXML
    public static void paint(Canvas canvas, Game game, boolean pause, Text text, int lvl, ArrayList<Pair<Double, Double>> board, int winScore) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setFill(Color.AQUA);
        context.fillRect(0, 0, 400, 400);
        context.setFill(new Color(1, 1, 1, 1));
        board.forEach((pair) -> {
            context.fillRect(pair.getKey(), pair.getValue(), SnakePart.getWidth(), SnakePart.getHeight());
        });
        Duration duration = Duration.millis(125);
        game.timeline = new Timeline(new KeyFrame(duration, (ActionEvent event) -> {
            if (game.getScore() >= winScore) {
                text.setText("Your score: " + String.valueOf(game.getScore()) + "  "
                        + "Your high score: " + game.getHighScore() + "    You win! Keep playing for a higher score!");
            } else {
                text.setText("Your score: " + String.valueOf(game.getScore()) + "  " + "Your high score: " + game.getHighScore());
            }
            if(!pause) {
                if (!game.isGameOver()) {
                    if(game.snake.gonePart != null) {
                        context.setFill(Color.AQUA);
                        context.fillRect(game.snake.gonePart.getX(), game.snake.gonePart.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                        context.setFill(SnakePart.getColor());
                        context.fillOval(game.snake.getSnake().get(0).getX(), game.snake.getSnake().get(0).getY(), SnakePart.getWidth(), SnakePart.getHeight());
                    }
                    context.setFill(Food.getColor());
                    context.fillRect(game.food.getX(), game.food.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                } else {
                    context.setFill(Color.BLACK);
                    context.setFont(new Font(24));
                    context.setTextAlign(TextAlignment.CENTER);
                    context.fillText("Game Over!", 200, 200);
                    game.timeline.stop();
                    if (game.getHighScore() < game.getScore()) {
                        helperMethods.setHighScore(1, game.getScore());
                    }
                    if (game.getScore() >= winScore) {
                        if (helperMethods.player.getCurrentUnlockedLevel() <= lvl) {
                            helperMethods.setCurrentLevel(lvl + 1);
                            helperMethods.player.setCurrentUnlockedLevel(lvl + 1);
                        }
                    }
                }
            }
        }));
        game.timeline.setCycleCount(Timeline.INDEFINITE);
        game.timeline.play();
    }



}
