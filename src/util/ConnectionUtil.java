package util;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionUtil {
    private static final String DB_URL = "jdbc:mysql://coms-319-057.cs.iastate.edu/userInfo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String DB_USERNAME = "team1";
    private static final String DB_PASSWORD = "comsVM@319";

    public static Connection connectDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
}
