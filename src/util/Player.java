package util;

public class Player {
    private String playerName;
    private int currentUnlockedLevel;
    private int freePlayHighScore;
    private int levelOneHighScore;
    private int levelTwoHighScore;
    private int levelThreeHighScore;

    public Player(String name, int freeHighScore, int oneHighScore, int twoHighScore, int threeHighScore, int level) {
        playerName = name;
        freePlayHighScore = freeHighScore;
        levelOneHighScore = oneHighScore;
        levelTwoHighScore = twoHighScore;
        levelThreeHighScore = threeHighScore;
        currentUnlockedLevel = level;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getFreePlayHighScore() {
        return freePlayHighScore;
    }

    public void setFreePlayHighScore(int freePlayHighScore) {
        this.freePlayHighScore = freePlayHighScore;
    }

    public int getLevelOneHighScore() {
        return levelOneHighScore;
    }

    public void setLevelOneHighScore(int levelOneHighScore) {
        this.levelOneHighScore = levelOneHighScore;
    }

    public int getLevelTwoHighScore() {
        return levelTwoHighScore;
    }

    public void setLevelTwoHighScore(int levelTwoHighScore) {
        this.levelTwoHighScore = levelTwoHighScore;
    }

    public int getLevelThreeHighScore() {
        return levelThreeHighScore;
    }

    public void setLevelThreeHighScore(int levelThreeHighScore) {
        this.levelThreeHighScore = levelThreeHighScore;
    }

    public int getCurrentUnlockedLevel() {
        return currentUnlockedLevel;
    }

    public void setCurrentUnlockedLevel(int currentUnlockedLevel) {
        this.currentUnlockedLevel = currentUnlockedLevel;
    }
}
