package screen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.ConnectionUtil;

import java.sql.Connection;

public class homeScreen extends Application {


    public static Connection connection;
    public static String player;

    @Override
    public void start(Stage stage) throws Exception {
        connection = ConnectionUtil.connectDB();
        Parent root = FXMLLoader.load(getClass().getResource("homeScreen.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
