package boards;

import javafx.util.Pair;

import java.util.ArrayList;

public final class ObstacleList {

    public static final ArrayList<Pair<Double, Double>> BOARD1 = new ArrayList<Pair<Double, Double>>() {
        {
            for (int i = 3; i < 7; i++) {
                for (int j = 3; j < 7; j++) {
                    add(new Pair((double) (i * 20), (double) (j * 20)));
                    add(new Pair((double) ((i + 10) * 20), (double) (j * 20)));
                    add(new Pair((double) (i * 20), (double) ((j + 10) * 20)));
                    add(new Pair((double) ((i + 10) * 20), (double) ((j + 10) * 20)));
                }
            }
        }
    };

    public static final ArrayList<Pair<Double, Double>> BOARD2 = new ArrayList<Pair<Double, Double>>() {
        {
            for(int j = 3; j < 17; j++){
                add(new Pair((double)(4*20), (double)(j*20)));
                add(new Pair((double)(15*20), (double)(j*20)));
            }
            for(int j = 4; j < 11; j++){
                add(new Pair((double)(9*20), (double)(j*20)));
                add(new Pair((double)(10*20), (double)(j*20)));
            }
            for(int i = 5; i < 15; i++){
                add(new Pair((double)(i*20), (double)(15*20)));
            }

            add(new Pair((double)(3*20), (double)(3*20)));
            add(new Pair((double)(3*20), (double)(4*20)));
            add(new Pair((double)(3*20), (double)(15*20)));
            add(new Pair((double)(3*20), (double)(16*20)));

            add(new Pair((double)(16*20), (double)(3*20)));
            add(new Pair((double)(16*20), (double)(4*20)));
            add(new Pair((double)(16*20), (double)(15*20)));
            add(new Pair((double)(16*20), (double)(16*20)));
        }
    };

    public static final ArrayList<Pair<Double, Double>> BOARD3 = new ArrayList<Pair<Double, Double>>() {
        {
            for(int i = 2; i < 19; i+=3) {
                for(int j = 3; j < 17; j++) {
                    add(new Pair((double) (i * 20), (double) (j * 20)));
                }
            }
        }
    };

    public static final ArrayList<Pair<Double, Double>> BOARD4 = new ArrayList<Pair<Double, Double>>() {
        {

            for(int j = 2; j < 16; j+=4){
                for(int y = j; y < j+2; y++){
                    add(new Pair((double)(3*20), (double)(y*20)));
                    add(new Pair((double)(4*20), (double)(y*20)));
                    add(new Pair((double)(11*20), (double)(y*20)));
                    add(new Pair((double)(12*20), (double)(y*20)));
                }
            }
            for(int j = 4; j < 18; j+=4){
                for(int y = j; y < j+2; y++){
                    add(new Pair((double)(7*20), (double)(y*20)));
                    add(new Pair((double)(8*20), (double)(y*20)));
                    add(new Pair((double)(15*20), (double)(y*20)));
                    add(new Pair((double)(16*20), (double)(y*20)));
                }
            }

        }
    };

    public static final ArrayList<Pair<Double, Double>> BOARD5 = new ArrayList<Pair<Double, Double>>() {
        {
            for(int j = 0; j < 22; j+=18){
                add(new Pair((double)(0*20), (double)(j*20)));
                add(new Pair((double)(1*20), (double)(j*20)));
                add(new Pair((double)(18*20), (double)(j*20)));
                add(new Pair((double)(19*20), (double)(j*20)));

                add(new Pair((double)(0*20), (double)((j+1)*20)));
                add(new Pair((double)(1*20), (double)((j+1)*20)));
                add(new Pair((double)(18*20), (double)((j+1)*20)));
                add(new Pair((double)(19*20), (double)((j+1)*20)));
            }
            for(int j = 5; j < 18; j+=8){
                add(new Pair((double)(0*20), (double)(j*20)));
                add(new Pair((double)(1*20), (double)(j*20)));
                add(new Pair((double)(2*20), (double)(j*20)));
                add(new Pair((double)(17*20), (double)(j*20)));
                add(new Pair((double)(18*20), (double)(j*20)));
                add(new Pair((double)(19*20), (double)(j*20)));

                add(new Pair((double)(0*20), (double)((j+1)*20)));
                add(new Pair((double)(1*20), (double)((j+1)*20)));
                add(new Pair((double)(2*20), (double)((j+1)*20)));
                add(new Pair((double)(17*20), (double)((j+1)*20)));
                add(new Pair((double)(18*20), (double)((j+1)*20)));
                add(new Pair((double)(19*20), (double)((j+1)*20)));
            }
            for(int i = 5; i < 18; i+=8){
                add(new Pair((double)(i*20), (double)(0*20)));
                add(new Pair((double)(i*20), (double)(1*20)));
                add(new Pair((double)(i*20), (double)(2*20)));
                add(new Pair((double)(i*20), (double)(17*20)));
                add(new Pair((double)(i*20), (double)(18*20)));
                add(new Pair((double)(i*20), (double)(19*20)));

                add(new Pair((double)((i+1)*20), (double)(0*20)));
                add(new Pair((double)((i+1)*20), (double)(1*20)));
                add(new Pair((double)((i+1)*20), (double)(2*20)));
                add(new Pair((double)((i+1)*20), (double)(17*20)));
                add(new Pair((double)((i+1)*20), (double)(18*20)));
                add(new Pair((double)((i+1)*20), (double)(19*20)));
            }
            for(int i = 9; i < 11; i++){
                add(new Pair((double)(i*20), (double)(4*20)));
                add(new Pair((double)(i*20), (double)(5*20)));
                add(new Pair((double)(i*20), (double)(6*20)));
                add(new Pair((double)(i*20), (double)(9*20)));
                add(new Pair((double)(i*20), (double)(10*20)));
                add(new Pair((double)(i*20), (double)(13*20)));
                add(new Pair((double)(i*20), (double)(14*20)));
                add(new Pair((double)(i*20), (double)(15*20)));
            }
            for(int j = 9; j < 11; j++){
                add(new Pair((double)(4*20), (double)(j*20)));
                add(new Pair((double)(5*20), (double)(j*20)));
                add(new Pair((double)(6*20), (double)(j*20)));
                add(new Pair((double)(13*20), (double)(j*20)));
                add(new Pair((double)(14*20), (double)(j*20)));
                add(new Pair((double)(15*20), (double)(j*20)));
            }
        }
    };

        public static final ArrayList<Pair<Double, Double>> FREEBOARD = new ArrayList<Pair<Double, Double>>() {
        {

        }
    };
}