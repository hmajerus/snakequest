package game;

import java.util.Random;

import boards.ObstacleList;
import javafx.scene.paint.Color;
import javafx.util.Pair;

public class Food extends SnakePart {

    private boolean eated;
    private static Color color=new Color(1, 1, 0 ,1);

    public Food(Snake snake) {
        super();
        setFoodLocation(snake);
        this.eated = false;
    }

    private void setFoodLocation(Snake snake){
        Random rand = new Random();
        int x = 0;
        int y = 0;
        boolean invalidLocation = true;
        while(invalidLocation){
            invalidLocation = false;
            x = rand.nextInt(380) / 20;
            y = rand.nextInt(380) / 20;
            for (Pair<Double, Double> pair : snake.board) {
                if(x*20 == pair.getKey() && y*20 == pair.getValue()){
                    invalidLocation = true;
                }
            }
            for ( SnakePart part : snake.getSnake()) {
                if(part.getX() == x*20 && part.getY() == y*20){
                    invalidLocation = true;
                }
            }
        }
        this.setX(x*SnakePart.getWidth());
        this.setY(y*SnakePart.getHeight());
    }

    public static Color getColor() {
        return color;
    }

    public static void setColor(Color color) {
        Food.color = color;
    }

    public boolean isEated() {
        return eated;
    }

    public void setEated(boolean eated) {
        this.eated = eated;
    }

}
