package game;

import javafx.animation.Timeline;
import javafx.geometry.Dimension2D;
import javafx.util.Pair;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Game {

    public Food food;
    public Timeline timeline;
    public final Snake snake;
    private int highScore;
    private final Dimension2D fieldSize;
    private int score;

    public int getScore() {
        return score;
    }

    public int getHighScore(){ return highScore; }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isGameOver() {
        if (snake.update(food, fieldSize.getWidth(), fieldSize.getHeight())) {
            if (food.isEated()) {
                score++;
                food = new Food(snake);
            }
            return false;
        }
        return true;
    }

    public Game(ArrayList<Pair<Double, Double>> brd, int level) {
        score = 0;
        fieldSize = new Dimension2D(400, 400);
        snake = new Snake(brd, level);
        food = new Food(snake);
        highScore = util.helperMethods.getHighScore(level);
    }
}