package game;

import boards.ObstacleList;
import javafx.util.Pair;

import java.util.ArrayList;

public class Snake {

    public ArrayList<Pair<Double, Double>> board;

    public Snake(ArrayList<Pair<Double, Double>> brd, int lvl) {
        SnakePart.setWidth(20);
        SnakePart.setHeight(20);
        SnakePart head = null;
        if(lvl == 2) {
            head = new SnakePart(160, 240);
        } else if(lvl == 3){
            head = new SnakePart(200, 260);
            setDirection(1);
        } else if(lvl == 4){
            head = new SnakePart(20, 20);
        } else if(lvl == 5){
            head = new SnakePart(20, 60);
        } else{
            head = new SnakePart(200, 200);
        }
        snake.add(head);
        board = brd;
        gonePart = null;
    }

    private ArrayList<SnakePart> snake = new ArrayList<>();
    private int direction;
    public SnakePart gonePart;  //Used to repaint the square the snake just left

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        if (snake.size() >= 2) {
            switch (direction) {
                case 0:
                    if (snake.get(0).getX() + 20 != snake.get(1).getX()) {
                        this.direction = direction;
                    }
                    break;
                case 1:
                    if (snake.get(0).getY() - 20 != snake.get(1).getY()) {
                        this.direction = direction;
                    }
                    break;
                case 2:
                    if (snake.get(0).getX() - 20 != snake.get(1).getX()) {
                        this.direction = direction;
                    }
                    break;
                case 3:
                    if (snake.get(0).getY() + 20 != snake.get(1).getY()) {
                        this.direction = direction;
                    }
                    break;

            }
        } else {
            this.direction = direction;
        }
    }

    public ArrayList<SnakePart> getSnake() {
        return snake;
    }

    public boolean isConflicted(double x, double y) {

        for (int j = snake.size() - 1; j > 0; --j) {
            if (snake.get(0).getX() == snake.get(j).getX()
                    && snake.get(0).getY() == snake.get(j).getY()) {
                return true;
            }
        }
        return snake.get(0).getX() == x
                || snake.get(0).getX() == (0 - SnakePart.getWidth())
                || snake.get(0).getY() == y
                || snake.get(0).getY() == (0 - SnakePart.getHeight());
    }

    public boolean update(Food food, double x, double y) {
        gonePart = new SnakePart(snake.get(snake.size() - 1).getX(), snake.get(snake.size() - 1).getY());
        for (int i = snake.size() - 1; i >= 0; --i) {
            if (i == 0) {
                switch (direction) {
                    case 0:
                        snake.get(0).setX(snake.get(0).getX() + 20);
                        break;
                    case 1:
                        snake.get(0).setY(snake.get(0).getY() - 20);
                        break;
                    case 2:
                        snake.get(0).setX(snake.get(0).getX() - 20);
                        break;
                    case 3:
                        snake.get(0).setY(snake.get(0).getY() + 20);
                        break;

                }

                if (isConflicted(x, y) || hitObstacle(x,y))
                    return false;

                if (snake.get(i).getX() == food.getX()
                        && snake.get(i).getY() == food.getY()) {
                    food.setEated(true);
                    SnakePart newSnakePart = new SnakePart
                            (snake.get(snake.size() - 1).getX(),
                                    snake.get(snake.size() - 1).getY());
                    snake.add(newSnakePart);
                    //gonePart = null;
                }
            } else {
                snake.get(i).setX(snake.get(i - 1).getX());
                snake.get(i).setY(snake.get(i - 1).getY());
            }
        }
        return true;
    }

    private boolean hitObstacle(double x, double y) {
        for (Pair<Double, Double> pair : board) {
            if(snake.get(0).getX() == pair.getKey() && snake.get(0).getY() == pair.getValue()){
                return true;
            }
        }
        return false;
    }
}