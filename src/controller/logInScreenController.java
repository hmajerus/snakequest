package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import util.helperMethods;

import java.net.URL;
import java.util.ResourceBundle;


public class logInScreenController implements Initializable {

    @FXML
    private TextField userName;

    @FXML
    private TextField textPassword;

    public void loginAction(ActionEvent event) {
        String username = userName.getText();
        String password = textPassword.getText();

        if (helperMethods.loginLogic(username, password)) {
            helperMethods.infoBox("Login Successful", null, "Success");
            helperMethods.nextScreen(event, "menuScreen.fxml");
        } else {
            helperMethods.infoBox("Please enter a valid username and password", null, "Failed");
        }
    }

    public void backAction(ActionEvent event) {
        helperMethods.nextScreen(event, "homeScreen.fxml");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
