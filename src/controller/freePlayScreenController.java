package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import boards.ObstacleList;
import game.Food;
import game.Game;
import game.SnakePart;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import util.helperMethods;

import java.io.FileNotFoundException;


public class freePlayScreenController {

    private Game game;
    private boolean pause = false;


    @FXML
    private Canvas canvas;
    @FXML
    private Text text;

    private Timeline timeline;

    //Image img = null;
    //img = new Image(new FileInputStream("C:\\Users\\David\\Desktop\\Classes\\SE319\\snakeQuest\\image\\snake.jpg"));

    @FXML
    private void returnAction(ActionEvent event) {
        try {
            String nextScreen = "menuScreen.fxml";
            helperMethods.nextScreen(event, nextScreen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void exitAction(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void newAction(ActionEvent event) throws FileNotFoundException {
        game = new Game(ObstacleList.FREEBOARD, 0);
        paint();
    }

    @FXML
    private void showColorPicker(ActionEvent event) {
        VBox box = new VBox();
        ColorPicker colorPicker = new ColorPicker();
        Button ok = new Button("Ok");

        box.getChildren().addAll(colorPicker, ok);
        Scene scene = new Scene(box);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        ok.setOnMouseClicked((MouseEvent event1) -> {
            SnakePart.setColor(colorPicker.getValue());
            stage.close();
        });

    }

    @FXML
    private void keyHandler(KeyEvent e) {
        int num = helperMethods.keyPressed(e);
        if (num == 4) {
            pause = !pause;
        } else if (!pause) {
            game.snake.setDirection(num);
        }
    }

    @FXML
    public void paint() {
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setFill(Color.AQUA);
        context.fillRect(0, 0, 400, 400);
        context.setFill(new Color(1, 1, 1, 1));
        ObstacleList.FREEBOARD.forEach((pair) -> {
            context.fillRect(pair.getKey(), pair.getValue(), SnakePart.getWidth(), SnakePart.getHeight());
        });

        Duration duration = Duration.millis(125);
        timeline = new Timeline(new KeyFrame(duration, (ActionEvent event) -> {
            text.setText("Your score: " + String.valueOf(game.getScore()) + "  " + "Your high score: " + game.getHighScore());
            if(!pause) {
                if (!game.isGameOver()) {
                    if(game.snake.gonePart != null) {
                        context.setFill(Color.AQUA);
                        context.fillRect(game.snake.gonePart.getX(), game.snake.gonePart.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                        context.setFill(SnakePart.getColor());
                        context.fillOval(game.snake.getSnake().get(0).getX(), game.snake.getSnake().get(0).getY(), SnakePart.getWidth(), SnakePart.getHeight());
                    }
                    context.setFill(Food.getColor());
                    context.fillOval(game.food.getX(), game.food.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                } else {
                    context.setFill(Color.BLACK);
                    context.setFont(new Font(24));
                    context.setTextAlign(TextAlignment.CENTER);
                    context.fillText("Game Over!", 200, 200);
                    timeline.stop();
                    if (game.getHighScore() < game.getScore()) {
                        helperMethods.setHighScore(0, game.getScore());
                    }
                }
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    @FXML
    public void repaint() throws FileNotFoundException {
        Duration duration = Duration.millis(125);
        timeline = new Timeline(new KeyFrame(duration, (ActionEvent event) -> {
            //TODO: Add high score from SQL
            text.setText("Your score: " + String.valueOf(game.getScore()) + "  " + "Your high score: " + game.getHighScore());
            GraphicsContext context = canvas.getGraphicsContext2D();
            if (!pause) {
                context.setFill(Color.AQUA);
                context.fillRect(0, 0, 400, 400);
                context.setFill(new Color(1, 1, 1, 1));
                ObstacleList.FREEBOARD.forEach((pair) -> {
                    context.fillRect(pair.getKey(), pair.getValue(), SnakePart.getWidth(), SnakePart.getHeight());
                });
                if (!game.isGameOver()) {
                    game.snake.getSnake().stream().forEach((part) -> {
                        context.setFill(SnakePart.getColor());
                        context.fillOval(part.getX(), part.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                    });
                    context.setFill(Food.getColor());
                    context.fillRect(game.food.getX(), game.food.getY(), SnakePart.getWidth(), SnakePart.getHeight());

                } else {
                    context.setFill(Color.BLACK);
                    context.setFont(new Font(24));
                    context.setTextAlign(TextAlignment.CENTER);
                    context.fillText("Game Over!", 200, 200);
                    timeline.stop();
                    if (game.getHighScore() < game.getScore()) {
                        helperMethods.setHighScore(0, game.getScore());
                    }
                }
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

}