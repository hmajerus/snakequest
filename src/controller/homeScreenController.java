package controller;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import util.helperMethods;

import java.net.URL;
import java.util.ResourceBundle;

public class homeScreenController implements Initializable {

    public void login(ActionEvent event) {

        try {
            String nextScreen = "logInScreen.fxml";
            helperMethods.nextScreen(event, nextScreen);
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    public void register(ActionEvent event) {

        try {
            helperMethods.nextScreen(event, "registerScreen.fxml");
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}

