package controller;

import boards.ObstacleList;
import game.Food;
import game.Game;
import game.SnakePart;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import util.helperMethods;

import java.io.FileNotFoundException;

public class levelFourScreenController {
    private Game game;
    private boolean pause = false;

    @FXML
    private Canvas canvas;
    @FXML
    private Text text;

    private Timeline timeline;

    @FXML
    private void returnAction(ActionEvent event) {
        try {
            String nextScreen = "menuScreen.fxml";
            helperMethods.nextScreen(event, nextScreen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void exitAction(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void newAction(ActionEvent event) throws FileNotFoundException {
        game = new Game(ObstacleList.BOARD4, 4);
        //repaint();
        paint();
        //helperMethods.paint(canvas, game, pause, text, 4, ObstacleList.BOARD4, 15);
    }

    @FXML
    private void showColorPicker(ActionEvent event) {
        VBox box = new VBox();
        ColorPicker colorPicker = new ColorPicker();
        Button ok = new Button("Ok");

        box.getChildren().addAll(colorPicker, ok);
        Scene scene = new Scene(box);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        ok.setOnMouseClicked((MouseEvent event1) -> {
            SnakePart.setColor(colorPicker.getValue());
            stage.close();
        });

    }

    @FXML
    private void keyHandler(KeyEvent e) {
        int num = helperMethods.keyPressed(e);
        if(num == 4) {
            pause = !pause;
        }
        else if (!pause) {
            game.snake.setDirection(num);
        }
    }

    @FXML
    public void paint() {
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setFill(Color.AQUA);
        context.fillRect(0, 0, 400, 400);
        context.setFill(new Color(1, 1, 1, 1));
        ObstacleList.BOARD4.forEach((pair) -> {
            context.fillRect(pair.getKey(), pair.getValue(), SnakePart.getWidth(), SnakePart.getHeight());
        });

        Duration duration = Duration.millis(125);
        timeline = new Timeline(new KeyFrame(duration, (ActionEvent event) -> {
            if (game.getScore() > 14) {
                text.setText("Your score: " + String.valueOf(game.getScore()) + "  "
                        + "Your high score: " + game.getHighScore() + "    You win! Keep playing for a higher score!");
            } else {
                text.setText("Your score: " + String.valueOf(game.getScore()) + "  " + "Your high score: " + game.getHighScore());
            }
            if(!pause) {
                if (!game.isGameOver()) {
                    if(game.snake.gonePart != null) {
                        context.setFill(Color.AQUA);
                        context.fillRect(game.snake.gonePart.getX(), game.snake.gonePart.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                        context.setFill(SnakePart.getColor());
                        context.fillOval(game.snake.getSnake().get(0).getX(), game.snake.getSnake().get(0).getY(), SnakePart.getWidth(), SnakePart.getHeight());
                    }
                    context.setFill(Food.getColor());
                    context.fillOval(game.food.getX(), game.food.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                } else {
                    context.setFill(Color.BLACK);
                    context.setFont(new Font(24));
                    context.setTextAlign(TextAlignment.CENTER);
                    context.fillText("Game Over!", 200, 200);
                    timeline.stop();
                    if (game.getHighScore() < game.getScore()) {
                        helperMethods.setHighScore(4, game.getScore());
                    }
                    if (game.getScore() > 14) {
                        if (helperMethods.player.getCurrentUnlockedLevel() <= 4) {
                            helperMethods.setCurrentLevel(5);
                            helperMethods.player.setCurrentUnlockedLevel(5);
                        }
                    }
                }
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    @FXML
    public void repaint() throws FileNotFoundException {

        Duration duration = Duration.millis(125);
        timeline = new Timeline(new KeyFrame(duration, (ActionEvent event) -> {
            //TODO: Add high score from SQL
            if (game.getScore() > 15) {
                text.setText("Your score: " + String.valueOf(game.getScore()) + "  "
                        + "Your high score: " + game.getHighScore() + "    You win! Keep playing for a higher score!");
            } else {
                text.setText("Your score: " + String.valueOf(game.getScore()) + "  " + "Your high score: " + game.getHighScore());
            }
            GraphicsContext context = canvas.getGraphicsContext2D();
            if(!pause) {
                context.setFill(Color.AQUA);
                context.fillRect(0, 0, 400, 400);
                context.setFill(new Color(1, 1, 1, 1));
                ObstacleList.BOARD1.forEach((pair) -> {
                    context.fillRect(pair.getKey(), pair.getValue(), SnakePart.getWidth(), SnakePart.getHeight());
                });
                if (!game.isGameOver()) {
                    game.snake.getSnake().stream().forEach((part) -> {
                        context.setFill(SnakePart.getColor());
                        context.fillOval(part.getX(), part.getY(), SnakePart.getWidth(), SnakePart.getHeight());
                    });
                    context.setFill(Food.getColor());
                    context.fillRect(game.food.getX(), game.food.getY(), SnakePart.getWidth(), SnakePart.getHeight());

                } else {
                    context.setFill(Color.BLACK);
                    context.setFont(new Font(24));
                    context.setTextAlign(TextAlignment.CENTER);
                    context.fillText("Game Over!", 200, 200);
                    timeline.stop();
                    if (game.getHighScore() < game.getScore()) {
                        helperMethods.setHighScore(1, game.getScore());
                    }
                    if (game.getScore() > 15) {
                        if (helperMethods.player.getCurrentUnlockedLevel() <= 1) {
                            helperMethods.setCurrentLevel(2);
                            helperMethods.player.setCurrentUnlockedLevel(2);
                        }
                    }
                }
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }
}
