package controller;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import util.helperMethods;

import java.net.URL;
import java.rmi.activation.ActivationInstantiator;
import java.util.ResourceBundle;

public class menuScreenController implements Initializable {

    public void freePlay(ActionEvent event) throws Exception {
        try {
            String nextScreen = "freePlayScreen.fxml";
            helperMethods.nextScreen(event, nextScreen);
            helperMethods.infoBox("To play: Press Start and use the arrow keys. Space will pause the game","Free Play","Free Play");

        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    public void levelOne(ActionEvent event) throws Exception {
        try {
            String nextScreen = "levelOneScreen.fxml";
            helperMethods.nextScreen(event, nextScreen);
            helperMethods.infoBox("To play: Press Start and use the arrow keys. To beat the level, reach a score of 15. Space will pause the game","Level One","Level One");

        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    public void levelTwo(ActionEvent event) throws Exception{
        if(helperMethods.player.getCurrentUnlockedLevel() >= 2) {
            try {
                String nextScreen = "levelTwoScreen.fxml";
                helperMethods.nextScreen(event, nextScreen);
                helperMethods.infoBox("To play: Press Start and use the arrow keys. To beat the level, reach a score of 15. Space will pause the game","Level Two","Level Two");

            } catch (
                    Exception e) {
                e.printStackTrace();
            }
        } else{
            helperMethods.infoBox("You need to first complete previous levels!", "Unsuccessful Operation", "Fail");
        }
    }

    public void levelThree(ActionEvent event) throws Exception{
        if(helperMethods.player.getCurrentUnlockedLevel() >= 3) {
            try {
                String nextScreen = "levelThreeScreen.fxml";
                helperMethods.nextScreen(event, nextScreen);
                helperMethods.infoBox("To play: Press Start and use the arrow keys. To beat the level, reach a score of 15. Space will pause the game","Level Three","Level Three");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else{
            helperMethods.infoBox("You need to first complete previous levels!", "Unsuccessful Operation", "Fail");
        }
    }

    public void levelFour(ActionEvent event) throws Exception{
        if(helperMethods.player.getCurrentUnlockedLevel() >= 4) {
            try {
                String nextScreen = "levelFourScreen.fxml";
                helperMethods.nextScreen(event, nextScreen);
                helperMethods.infoBox("To play: Press Start and use the arrow keys. To beat the level, reach a score of 15. Space will pause the game","Level Three","Level Three");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else{
            helperMethods.infoBox("You need to first complete previous levels!", "Unsuccessful Operation", "Fail");
        }
    }

    public void levelFive(ActionEvent event) throws Exception{
        if(helperMethods.player.getCurrentUnlockedLevel() >= 5) {
            try {
                String nextScreen = "levelFiveScreen.fxml";
                helperMethods.nextScreen(event, nextScreen);
                helperMethods.infoBox("To play: Press Start and use the arrow keys. To beat the level, reach a score of 15. Space will pause the game","Level Three","Level Three");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else{
            helperMethods.infoBox("You need to first complete previous levels!", "Unsuccessful Operation", "Fail");
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
