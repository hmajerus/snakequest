package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import util.helperMethods;

import java.net.URL;
import java.util.ResourceBundle;

public class registerScreenController implements Initializable {
    @FXML
    private TextField userName;

    @FXML
    private TextField textPassword;

    public void registerAction(ActionEvent event) {
        String username = userName.getText();
        String password = textPassword.getText();

        if (password.length() > 3 && password.length() < 21) {
            if (helperMethods.registerLogic(username, password)) {
                helperMethods.infoBox("New account created", "Successful Register", "Success");
                helperMethods.nextScreen(event, "menuScreen.fxml");
                screen.homeScreen.player = username;
            } else {
                helperMethods.infoBox("Username already exists", "Unsuccessful Register", "Invalid Username");
            }
        } else {
            helperMethods.infoBox("Password does not meet requirements. Please make it 4-20 characters long", "Unsuccessful Register", "Invalid Password");
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void backAction(ActionEvent event) {
        helperMethods.nextScreen(event, "homeScreen.fxml");
    }
}
