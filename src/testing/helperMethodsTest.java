package testing;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import screen.homeScreen;
import util.helperMethods;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class helperMethodsTest {
    @Before
    public void setUp() {
        homeScreen.connection = util.ConnectionUtil.connectDB();
    }

    @After
    public void tearDown() throws Exception {
        homeScreen.connection.close();
    }

    @Test
    public void loginLogicSuccessfulTest1() {
        String user1 = "test1";
        String password1 = "password";
        assertTrue(helperMethods.loginLogic(user1, password1));
    }

    @Test
    public void loginLogicSuccessfulTest2() {
        String user1 = "davidhayes";
        String password1 = "password";
        assertTrue(helperMethods.loginLogic(user1, password1));
    }

    @Test
    public void loginLogicUnsuccessfulTest1() {
        String user1 = "test1";
        String password1 = "negative";
        assertFalse(helperMethods.loginLogic(user1, password1));
    }

    @Test
    public void loginLogicUnsuccessfulTest2() {
        String user1 = "davidhayes";
        String password1 = "negative";
        assertFalse(helperMethods.loginLogic(user1, password1));
    }
}